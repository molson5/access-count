#pragma once
#include <time.h> /* For rand */
#include <stdint.h>

typedef struct node node;
typedef struct node {
  node *next, *prev;
  char *data;
} node;

uint64_t rand_uint64() {
  int i;
  uint64_t r;
  
  r = 0;
  srand(time(0));
  for(i = 0; i < 64; i += 30) {
    r = r * ((uint64_t) RAND_MAX + 1) + rand();
  }
  return r;
}

char coin_flip() {
  char random;
  srand(time(0));
  random = rand() % 2;
  return random;
}

node *linked_list_init(size_t num_nodes, size_t node_size) {
  size_t i;
  node *tmp, *prev_tmp, *root;
  
  tmp = NULL;
  prev_tmp = NULL;
  for(i = 0; i < num_nodes; i++) {
    /* This is the allocation site for a node struct.
       We'll ignore this in the profiling, but use these pointers
       to shuffle the nodes for cache-unfriendliness. */
    tmp = malloc(sizeof(node));
    if(i == 0) {
      root = tmp;
      tmp->prev = NULL;
    }
    if(prev_tmp) {
      tmp->prev = prev_tmp;
      prev_tmp->next = tmp;
    }
    prev_tmp = tmp;
    
    /* This is the allocation site that we're actually going to
       be counting accesses to. */
    tmp->data = malloc(sizeof(char) * node_size);
  }
  prev_tmp->next = NULL;
  
  return root;
}

void linked_list_free(node *root) {
  node *tmp, *prev_tmp;
  
  if(!root) {
    return;
  }
  
  prev_tmp = NULL;
  tmp = root;
  while(tmp->next) {
    if(prev_tmp) {
      if(prev_tmp->data) {
        free(prev_tmp->data);
      }
      free(prev_tmp);
    }
    prev_tmp = tmp;
    tmp = tmp->next;
  }
  if(prev_tmp) {
    if(prev_tmp->data) {
      free(prev_tmp->data);
    }
    free(prev_tmp);
  }
  if(tmp->data) {
    free(tmp->data);
  }
  free(tmp);
}

void linked_list_print(node *root) {
  if(!root) {
    printf("=====\n");
    printf("=====\n");
    return;
  }
  
  printf("=====\n");
  while(root) {
    printf("%p: (prev: %p), (next: %p)\n", root, root->prev, root->next);
    root = root->next;
  }
  printf("=====\n");
}

void linked_list_rprint(node *root) {
  if(!root) {
    printf("=====\n");
    printf("=====\n");
    return;
  }
  
  while(root->next) {
    root = root->next;
  }
  
  printf("=====\n");
  while(root) {
    printf("%p: (prev: %p), (next: %p)\n", root, root->prev, root->next);
    root = root->prev;
  }
  printf("=====\n");
}

size_t linked_list_size(node *root) {
  size_t retval;
  
  retval = 0;
  while(root) {
    retval++;
    root = root->next;
  }
  
  return retval;
}

void linked_list_disconnect_node(node *root) {
  if(root->prev) {
    root->prev->next = root->next;
  }
  if(root->next) {
    root->next->prev = root->prev;
  }
}

/* Inserts `src` before `dst`. Returns the new root of the list that contains `dst`. */
node *linked_list_insert_before(node *src, node *dst) {
  node *root, *tmp;
  
  if(dst->prev) {
    dst->prev->next = src;
  }
  tmp = dst->prev;
  dst->prev = src;
  src->prev = tmp;
  src->next = dst;
  
  root = dst;
  while(root->prev) {
    root = root->prev;
  }
  
  return root;
}

/* Moves a single node, `*src`, to the end of `*dst`. Updates all pointers. */
void linked_list_move_one(node **src, node **dst) {
  node *tmp, *next_in_src;
  
  if(!(*src)) {
    fprintf(stderr, "Tried to move a NULL linked_list.\n");
    return;
  }
  
  linked_list_disconnect_node(*src);
  next_in_src = (*src)->next;
  (*src)->next = NULL;
  
  if(!(*dst)) {
    *dst = *src;
    (*src)->prev = NULL;
  } else {
    /* Get a pointer to the last node in `dst` */
    tmp = *dst;
    while(tmp->next) {
      tmp = tmp->next;
    }
    tmp->next = *src;
    (*src)->prev = tmp;
  }
  
  *src = next_in_src;
}

void linked_list_shuffle_recurse(node **root) {
  node *tmp, *dummy, *tmp1, *tmp2;
  size_t random, i, max;
  char coin;
  
  if((*root)->next == NULL) {
    return;
  }
  
  /* Split `root` between `tmp1` and `tmp2` */
  tmp1 = NULL;
  tmp2 = NULL;
  while(*root) {
    linked_list_move_one(root, &tmp1);
    if(*root) {
      linked_list_move_one(root, &tmp2);
    }
  }
  
  linked_list_shuffle_recurse(&tmp1);
  linked_list_shuffle_recurse(&tmp2);
  
  /* Insert a random dummy node into `tmp2`. */
  dummy = NULL;
  if(linked_list_size(tmp2) < linked_list_size(tmp1)) {
    max = linked_list_size(tmp2) - 1;
    random = rand_uint64() % (max + 1);
    tmp = tmp2;
    for(i = 0; i < random; i++) {
      tmp = tmp->next;
    }
    dummy = malloc(sizeof(node));
    dummy->data = NULL;
    tmp2 = linked_list_insert_before(dummy, tmp);
  }
  
  /* Merge the two lists into the root list */
  while(linked_list_size(tmp2) && linked_list_size(tmp1)) {
    char coin = coin_flip();
    if(coin) {
      linked_list_move_one(&tmp1, root);
    } else {
      linked_list_move_one(&tmp2, root);
    }
  }
  while(tmp1) {
    linked_list_move_one(&tmp1, root);
  }
  while(tmp2) {
    linked_list_move_one(&tmp2, root);
  }
}

void linked_list_shuffle(node **root) {
  node *tmp, *tmp_next;
  
  /* Call the recursive function to shuffle */
  linked_list_shuffle_recurse(root);
  
  /* Free the dummy nodes that we created */
  tmp = *root;
  while(tmp) {
    if(tmp->data == NULL) {
      /* it's a dummy node, free it */
      tmp_next = tmp->next;
      linked_list_disconnect_node(tmp);
      free(tmp);
      if(tmp == *root) {
        *root = tmp_next;
      }
    } else {
      tmp_next = tmp->next;
    }
    tmp = tmp_next;
  }
  
  linked_list_print(*root);
}

/* Fills the data in the linked_list with random data, using the specified
   number of threads. Spins up and frees up the threads. */
void linked_list_random_fill(node *root, size_t node_size, size_t num_threads) {
  size_t i;
  node *tmp;
  
  if((num_threads == 0) || (num_threads > 512)) {
    fprintf(stderr, "Unreasonable number of threads given.\n");
    return;
  }
  
  if(num_threads == 1) {
    for(i = 0; i < node_size; i++) {
      tmp = root;
      while(tmp) {
        tmp->data[i] = 42; /* This is random, fight me */
        tmp = tmp->next;
      }
    }
  } else {
    fprintf(stderr, "Multiple threads not supported quite yet.\n");
    return;
  }
}

/* Randomly accesses (reads) the bytes in each linked list element. */
void linked_list_random_access(node *root, size_t node_size, size_t chunks, size_t num_threads) {
  size_t offset, sum, stride, chunk, chunk_backwards, chunk_forwards;
  node *tmp;
  char flip;
  
  if((num_threads == 0) || (num_threads > 512)) {
    fprintf(stderr, "Unreasonable number of threads given.\n");
    return;
  }
  
  stride = node_size / chunks;
  flip = 1; /* We'll use this to flip/flop between the back and front of the node's data */
  
  if(num_threads == 1) {
    for(offset = 0; offset < stride; offset++) {
      chunk_backwards = chunks - 1;
      chunk_forwards = 0;
      while(1) {
        if(chunk_forwards == chunk_backwards) {
          break;
        }
        if(flip) {
          chunk = chunk_forwards;
        } else {
          chunk = chunk_backwards;
        }
        tmp = root;
        printf("Accessing byte %zu\n", offset + (chunk * stride));
        while(tmp) {
          sum += tmp->data[offset + (chunk * stride)];
          tmp = tmp->next;
        }
        if(flip) {
          chunk_forwards++;
          flip = 0;
        } else {
          chunk_backwards--;
          flip = 1;
        }
      }
    }
  } else {
    fprintf(stderr, "Multiple threads not supported quite yet.\n");
    return;
  }
}
